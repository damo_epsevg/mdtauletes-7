package damo.cs.upc.edu.fragments;

import android.app.Fragment;
import android.content.Context;

/**
 * Created by josepm on 27/9/16.
 */
public class FragmentObservable extends Fragment {

    public static final int NOVA_SELECCIÖ = 100;
    public static final int MODIF_CRIM = 110;
    public static final int EDICIO_CRIM = 120;

    private ObservadorFragment observador;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        observador = (ObservadorFragment) getActivity();
    }

    public void setObservadorFragment(ObservadorFragment observadorFragment) {
        observador = observadorFragment;
    }

    protected void avisaObservador(Crim crim, int idCrida) {
        if (observador == null) return;
        observador.onCanviFragment(crim, idCrida);
    }

    @Override
    public void onDetach() {
        super.onDetach();
        observador = null;
    }

    public interface ObservadorFragment {
        void onCanviFragment(Crim crim, int idCrida);
    }
}
